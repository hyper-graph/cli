import {
	BaseTypescriptProgram,
	TypescriptOptions,
} from './base.typescript.program';

type Options = {
	watch?: boolean;
}
export class TypescriptProgram extends BaseTypescriptProgram<Options> {
	public get hasCjsConfig(): boolean {
		return this.typescriptConfig.cjsPath !== null;
	}
	protected getArgs(options: Options & TypescriptOptions): string[] {
		const { watch } = options;
		const args = this.getCommonArgs(options);

		if (watch) {
			args.push('--watch');
		}

		return args;
	}

	protected getCommandName(): string {
		return 'tsc';
	}
}
