import { Inject } from '@nestjs/common';

import { PackageConfig } from '../../config';

import { BaseProgram } from './base.program';

type MochaOptions = {
	pattern?: string;
};

export class MochaProgram extends BaseProgram<MochaOptions> {
	@Inject()
	protected readonly packageConfig: PackageConfig;

	protected getArgs(options: MochaOptions): string[] {
		const { pattern } = options;

		const args: string[] = [];

		if (pattern) {
			args.push(pattern);
		}

		return args;
	}

	protected getEnvs(_options: MochaOptions): Record<string, string> {
		return {
			...super.getEnvs(_options),
			HG_ENV: 'test',
		};
	}

	protected getCommandName(): string {
		return 'mocha';
	}
}
