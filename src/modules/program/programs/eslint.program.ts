import { Inject } from '@nestjs/common';
import { createWriteStream } from 'fs';
import isCi from 'is-ci';
import { resolve } from 'path';

import type { ProcessInfo } from '../../../utils';

import { TypescriptConfig } from '../../config';
import { BaseProgram } from './base.program';

type EslintOptions = {
	fix?: boolean;
};

export class EslintProgram extends BaseProgram<EslintOptions> {
	@Inject()
	protected typescriptConfig: TypescriptConfig;

	protected getArgs(options: EslintOptions): string[] {
		const { fix } = options;

		const args = [
			'--ext',
			'.ts,.tsx',
			'--cache',
			'--report-unused-disable-directives',
		];

		if (fix) {
			args.push('--fix');
		}

		if (isCi) {
			args.push('--max-warnings', '0');
			args.push('--format', 'junit');
		} else {
			args.push('--format', 'codeframe');
		}

		return [...args, this.typescriptConfig.rootDir];
	}

	protected runCommand(command: string, args: string[], env: Record<string, string>): ProcessInfo {
		const processInfo: ProcessInfo = super.runCommand(command, args, env);
		const { stdout: childStdout } = processInfo.process;

		if (isCi && childStdout) {
			const junitOutputFile = createWriteStream(resolve('eslint.junit.xml'));
			childStdout.unpipe(process.stdout);
			childStdout.pipe(junitOutputFile);
			childStdout.once('end', () => junitOutputFile.end());
		}

		return processInfo;
	}

	protected getCommandName(): string {
		return 'eslint';
	}
}
