import {
	BaseTypescriptProgram,
	TypescriptOptions,
} from './base.typescript.program';

type Options = {
	onSuccess?: string;
}
export class TypescriptWatchProgram extends BaseTypescriptProgram<Options> {
	protected getArgs(options: Options & TypescriptOptions): string[] {
		let { onSuccess } = options;
		const args = [...this.getCommonArgs(options), '--noClear'];

		if (onSuccess) {
			if (process.env.NODE_OPTIONS) {
				onSuccess = `cross-env NODE_OPTIONS='${process.env.NODE_OPTIONS}' ${onSuccess}`;
			}
			args.push('--onSuccess', onSuccess);
		}

		return args;
	}

	protected getCommandName(): string {
		return 'tsc-watch';
	}
}
