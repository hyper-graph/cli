import { Logger } from '@nestjs/common';

import { exec, ProcessInfo } from '../../../utils';

export abstract class BaseProgram<OptionsType = {}> {
	private readonly logger = new Logger();

	public async exec(options: OptionsType): Promise<number> {
		await this.beforeExec(options);

		const commandName = this.getCommandName();
		const scriptArgs = [commandName, ...this.getArgs(options)];
		const env = this.getEnvs(options);

		const exitCode: number = await this.runCommand('yarn', scriptArgs, env);

		await this.afterExec(options, exitCode);

		return exitCode;
	}

	protected async beforeExec(_options: OptionsType): Promise<void> {}
	protected runCommand(command: string, args: string[], env: Record<string, string>): ProcessInfo {
		if (!process.platform.startsWith('win')) {
			return exec(command, args, env);
		}
		return exec('cmd', ['/s', '/c', command, ...args], env);
	}
	protected async afterExec(_options: OptionsType, exitCode: number): Promise<void> {
		this.logger.log(`Process exit with code ${exitCode}`, Object.getPrototypeOf(this).constructor.name);
	}

	protected getEnvs(_options: OptionsType): Record<string, string> {
		return { NODE_OPTIONS: '' };
	}

	protected abstract getArgs(options: OptionsType): string[];
	protected abstract getCommandName(): string;
}
