import type { OnModuleInit } from '@nestjs/common';
import type { IPackageJson } from '@ts-type/package-dts';
import { promises as fs } from 'fs';
import { resolve } from 'path';

export class PackageConfig implements OnModuleInit {
	private type: 'commonjs' | 'module' = 'commonjs';

	public async onModuleInit(): Promise<void> {
		const packageJson = await PackageConfig.loadPackageJson();

		if (packageJson.type === 'module') {
			this.type = 'module';
		}
	}

	public get isModule(): boolean {
		return this.type === 'module';
	}

	public get isCommonJS(): boolean {
		return this.type === 'commonjs';
	}

	private static async loadPackageJson(): Promise<IPackageJson> {
		const path = resolve(process.cwd(), 'package.json');
		const content = await fs.readFile(path, 'utf-8');

		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return JSON.parse(content);
	}
}
