import type { OnModuleInit } from '@nestjs/common';
import { strict as assert } from 'assert';
import { resolve } from 'path';
import type { CompilerOptions } from 'typescript';

import { exists } from '../../utils';

type BaseGetPathToTsConfigOptions = {
	postfix?: string;
	ignoreNotFound?: boolean;
};
type GetPathToTsConfigOptionsWithoutIgnore = BaseGetPathToTsConfigOptions & {
	ignoreNotFound?: false;
};
type GetPathToTsConfigOptionsWithIgnore = BaseGetPathToTsConfigOptions & {
	ignoreNotFound: true;
};

type GetPathToTsConfigOptions = GetPathToTsConfigOptionsWithIgnore | GetPathToTsConfigOptionsWithoutIgnore;

export class TypescriptConfig implements OnModuleInit {
	private esmCompilerOptions: CompilerOptions;
	private cjsCompilerOptions: CompilerOptions | null;
	private pathToEsmTsConfig: string;
	private pathToCjsTsConfig: string | null;

	public get hasDefaultConfigEsmTarget(): boolean {
		const moduleKind: string = this.esmCompilerOptions.module as any ?? 'CommonJS';

		return moduleKind.toLowerCase() === 'ESNext'.toLowerCase();
	}

	public async onModuleInit(): Promise<void> {
		this.pathToEsmTsConfig = await this.getPathToTsConfig();
		this.pathToCjsTsConfig = await this.getPathToTsConfig({
			postfix: 'commonjs',
			ignoreNotFound: true,
		});

		this.esmCompilerOptions = await TypescriptConfig.loadCompilerOptions(this.pathToEsmTsConfig);
		this.cjsCompilerOptions = this.pathToCjsTsConfig
			? await TypescriptConfig.loadCompilerOptions(this.pathToCjsTsConfig)
			: null;
	}

	public get esmPath(): string {
		return this.pathToEsmTsConfig;
	}

	public get cjsPath(): string | null {
		return this.pathToCjsTsConfig;
	}

	public get rootDir(): string {
		const { rootDir: esRootDir } = this.esmCompilerOptions;

		assert.ok(esRootDir, 'Field rootDir required in tsconfig.json');

		if (this.cjsCompilerOptions) {
			const { rootDir: cjsRootDir } = this.cjsCompilerOptions;

			assert.ok(cjsRootDir === esRootDir, 'Root dir in tsconfigs for esm and cjs will have same values');
		}

		return esRootDir;
	}

	public get outDir(): string {
		const { outDir } = this.esmCompilerOptions;

		assert.ok(outDir, 'Field outDir required in tsconfig.json');

		return outDir;
	}

	private async getPathToTsConfig(options?: GetPathToTsConfigOptionsWithoutIgnore): Promise<string>;
	private async getPathToTsConfig(options: GetPathToTsConfigOptionsWithIgnore): Promise<string | null>;
	private async getPathToTsConfig(options: GetPathToTsConfigOptions = {}): Promise<string | null> {
		const { postfix = '', ignoreNotFound = false } = options;
		const postfixWithDelimiter = postfix ? `.${postfix}` : '';

		const cwd = process.cwd();
		const buildConfigPath = resolve(cwd, `tsconfig${postfixWithDelimiter}.build.json`);
		const commonConfigPath = resolve(cwd, `tsconfig${postfixWithDelimiter}.json`);

		const buildConfigExists = await exists(buildConfigPath);

		if (buildConfigExists) {
			return buildConfigPath;
		}

		const commonConfigExists = await exists(commonConfigPath);

		if (commonConfigExists) {
			return commonConfigPath;
		}

		if (ignoreNotFound) {
			return null;
		}
		throw new Error('Tsconfig not found');
	}

	private static async loadCompilerOptions(path: string): Promise<CompilerOptions> {
		let parent: string | null = path;
		let compilerOptions: CompilerOptions = {};

		while (parent !== null) {
			const { default: content } = await import(parent) as { default: any };

			compilerOptions = {
				...content.compilerOptions,
				...compilerOptions,
			};
			parent = content.extends ?? null;
		}

		return compilerOptions;
	}
}
