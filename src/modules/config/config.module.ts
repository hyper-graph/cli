import {
	Global,
	Module,
} from '@nestjs/common';
import { PackageConfig } from './package.config';
import { TypescriptConfig } from './typescript.config';

const configs = [TypescriptConfig, PackageConfig];

@Global()
@Module({
	providers: configs,
	exports: configs,
})
export class ConfigModule {}
