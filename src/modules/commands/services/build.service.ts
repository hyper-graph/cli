import { Inject } from '@nestjs/common';
import {
	Command,
	Option,
} from 'nestjs-command';

import {
	TypescriptProgram,
	TypescriptWatchProgram,
} from '../../program';

const CleanOption = Option({
	name: 'clean',
	alias: 'c',
	type: 'boolean',
	requiresArg: false,
	default: false,
});

const WatchOption = Option({
	name: 'watch',
	alias: 'w',
	type: 'boolean',
	requiresArg: false,
	default: false,
});

const OnSuccessOption = Option({
	name: 'onSuccess',
	type: 'string',
});

export class BuildService {
	@Inject()
	protected readonly typescriptProgram: TypescriptProgram;
	@Inject()
	protected readonly typescriptWatchProgram: TypescriptWatchProgram;

	@Command({ command: 'build' })
	public async build(
		@CleanOption clean: boolean,
		@WatchOption watch: boolean,
	): Promise<number> {
		const esmExitCode = await this.typescriptProgram.exec({ clean, watch });

		const cjsExitCode = this.typescriptProgram.hasCjsConfig
			? await this.typescriptProgram.exec({ clean, watch, commonjs: true })
			: 0;

		return Math.max(esmExitCode, cjsExitCode);
	}

	@Command({ command: 'build:dev' })
	public async buildDev(
		@CleanOption clean: boolean,
		@OnSuccessOption onSuccess: string,
	): Promise<number> {
		return this.typescriptWatchProgram.exec({ clean, onSuccess });
	}
}
