import { Module } from '@nestjs/common';
import { CommandModule } from 'nestjs-command';

import { CommandsModule } from './modules';

@Module({ imports: [CommandModule, CommandsModule] })
export class CliModule {}
