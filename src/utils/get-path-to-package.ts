import { dirname, resolve } from 'path';
import { stat } from 'fs/promises';

const cwd = process.cwd();

export async function getPathToPackage(packageName: string): Promise<string | null> {
	const paths = [];
	let restPath = cwd;

	while (restPath !== '/') {
		paths.push(restPath);

		restPath = dirname(restPath);
	}

	let availablePath: string | null = null;

	for (const path of paths) {
		const currentPath = resolve(path, 'node_modules', packageName);
		const info = await stat(currentPath)
			.catch(() => null);

		if (info?.isDirectory()) {
			availablePath = currentPath;
			break;
		}
	}

	return availablePath;
}
