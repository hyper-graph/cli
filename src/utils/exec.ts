import { ChildProcess, spawn } from 'child_process';

export type ProcessInfo = PromiseLike<number> & {
	process: ChildProcess;
}

export function exec(command: string, args: string[] = [], env: Record<string, string> = {}): ProcessInfo {
	const childProcess = spawn(command, args, {
		env: {
			...process.env,
			...env,
		},
	});

	childProcess.stderr.pipe(process.stderr);
	childProcess.stdout.pipe(process.stdout);

	const promise = new Promise<number>(resolve => {
		childProcess.once('exit', resolve);
	});

	return {
		process: childProcess,
		then: promise.then.bind(promise),
	};
}
