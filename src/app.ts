#!/usr/bin/env node
import {
	INestApplicationContext,
	Logger,
	LogLevel,
} from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { CommandService } from 'nestjs-command';

import { CliModule } from './cli.module';

const logLevels: LogLevel[] = ['debug', 'verbose', 'log', 'warn', 'error'];

async function getApp(): Promise<INestApplicationContext> {
	const app = await NestFactory.createApplicationContext(CliModule, {
		logger: ['warn', 'error'],
		bufferLogs: false,
		autoFlushLogs: true,
		abortOnError: false,
	});

	await app.init();

	Logger.flush();
	Logger.overrideLogger(logLevels.filter(level => level !== 'debug'));

	if (process.env.HG_DEBUG) {
		app.useLogger(logLevels);
	}

	return app;
}

async function main(): Promise<void> {
	const app = await getApp();
	const commandService = app.get(CommandService);

	const { yargs } = commandService;

	yargs
		.scriptName('hg')
		.skipValidation('----')
		.option('completion', {
			type: 'boolean',
			default: false,
		})
		.option('log-level', {
			alias: 'l',
			type: 'string',
			requiresArg: false,
			default: 'warn' as LogLevel,
		})
		.coerce('log-level', arg => {
			if (logLevels.includes(arg)) {
				const requestedLogLevels = logLevels.slice(logLevels.indexOf(arg));

				app.useLogger(requestedLogLevels);
			}
		})
		.coerce('completion', arg => {
			if (arg) {
				yargs.showCompletionScript();
				process.exit(0);
			}
		});

	commandService.exec();
}

main().catch(error => {
	console.error(error);

	process.exit(1);
});
