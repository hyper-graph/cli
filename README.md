# HG CLI
## Why?
You won't copy your scripts from package to package.

You can use hg command for all your packages in workspace.

---

*NOTE*

Works with pnp!

---

## Commands
### Common options
* `--log-level value`, `-l value` - possible values are (debug, verbose, log, warn, error). warn is default
### build
Will run typescript compiler from current directory.

For typescript config will be used `tsconfig.build.json` if exists or `tsconfig.json` as fallback.

```shell
hg build
```

* `--clean`, `-c` - remove outDir before compilation
* `--watch`, `-w` - will run typescript in watch mode. Use build:dev command instead.

### build:dev
Same as `build -w` with watch mode via `tsc-watch`.

```shell
hg build:dev
```

* `--clean`, `-c` - remove outDir before compilation.
* `--onSuccess script` - will run script after each success compilations.

### lint
Run eslint from current directory.

* Enable cache by default.
* Add typescript files extensions.
* Use `codeframe` formatter by default.

### lint:fix
Run `lint` command with eslint `--fix` flag.
